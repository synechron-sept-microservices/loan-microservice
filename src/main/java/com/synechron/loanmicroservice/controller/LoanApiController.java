package com.synechron.loanmicroservice.controller;

import com.synechron.loanmicroservice.model.Customer;
import com.synechron.loanmicroservice.model.Loan;
import com.synechron.loanmicroservice.repository.CustomerRepository;
import com.synechron.loanmicroservice.repository.LoanRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/customer/")
@Slf4j
@RequiredArgsConstructor
public class LoanApiController {
    private final CustomerRepository customerRepository;
    private final LoanRepository loanRepository;
    private final Environment environment;

    @PostMapping("/{customerId}/loans")
    public Loan applyForLoan(@PathVariable("customerId")long customerId, @RequestBody Loan loan){
        log.info("Came inside the apply for loan inside the Loan API Controller .... {}", environment.getProperty("server.port"));
        final Optional<Customer> optionalCustomer = this.customerRepository.findById(customerId);
        if(optionalCustomer.isPresent()){
            Customer customer = optionalCustomer.get();
            customer.getLoans().add(loan);
            loan.setCustomer(customer);
            Customer savedCustomer = this.customerRepository.save(customer);
            return savedCustomer
                        .getLoans()
                        .stream()
                        .filter(loan1 -> loan1.getLoanAmount() == loan.getLoanAmount())
                        .findFirst()
                        .orElseThrow(()-> new IllegalArgumentException("Unable to disburse loan amount"));

        }
        String loanId = UUID.randomUUID().toString();
        loan.setLoanId(loanId);
        loan.setStatus("DISBURSED");
        loan.setBalanceAmount(loan.getLoanAmount());
        loan.setBalanceTenure(loan.getTotalTenure());
        return loan;
    }

    @GetMapping("/{customerId}/loans")
    public Set<Loan> fetchLoansByCustomerId(@PathVariable("customerId")long customerId){
        final Optional<Customer> optionalCustomer = this.customerRepository.findById(customerId);
        if(optionalCustomer.isPresent()){
            Customer customer = optionalCustomer.get();
            return customer.getLoans();
        }
        return new HashSet<>();
    }

    @GetMapping("/loans/{loanId}")
    public Loan fetchLoanByLoanId(@PathVariable("loanId") String loanId){
       return this.loanRepository.findById(loanId).orElseThrow(() -> new IllegalArgumentException("Invalid loan id"));
    }
}