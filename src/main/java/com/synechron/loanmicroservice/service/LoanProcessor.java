package com.synechron.loanmicroservice.service;

import com.synechron.loanmicroservice.model.Loan;
import com.synechron.loanmicroservice.repository.LoanRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class LoanProcessor {

    private final LoanRepository loanRepository;

    @StreamListener(Sink.INPUT)
    public void processLoan(Loan loan){
      log.info("Processing the loan application :: ");
      log.info(loan.toString());
      log.info("Processing completed for the loan application");
      loan.setLoanId(UUID.randomUUID().toString());
      this.loanRepository.save(loan);
    }
}