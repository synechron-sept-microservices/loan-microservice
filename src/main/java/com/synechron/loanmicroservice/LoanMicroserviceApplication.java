package com.synechron.loanmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.EnableMBeanExport;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(Sink.class)
public class LoanMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoanMicroserviceApplication.class, args);
    }

}
